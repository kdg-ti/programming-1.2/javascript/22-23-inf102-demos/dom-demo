import {MOUNTAINS} from "./data.js"

function buildTable(data) {
  const table = document.createElement("table");

  const fields = Object.keys(data[0]);
  const headRow = document.createElement("tr");
  headRow.append(...fields.map(name => createElementWithText("th", name)))
  table.append(headRow);

  data.forEach(object => appendRow(object,fields,table));

  return table;
}

function appendRow(object,fields,table) {
  const row = document.createElement("tr");
  row.append(...fields.map(name => createCellWithText(object[name])))
  table.append(row);
}

function createCellWithText(text) {
  const cell = createElementWithText("td", text)
  if (typeof text == "number") {
    cell.classList.add("numeric-cell")
  }
  return cell
}

function createElementWithText(type, text) {
  const element = document.createElement(type);
  element.append(document.createTextNode(text));
  return element;
}

function setMountains() {
  document.querySelector("#mountains")
    .replaceChildren(buildTable(MOUNTAINS));
}

setMountains()