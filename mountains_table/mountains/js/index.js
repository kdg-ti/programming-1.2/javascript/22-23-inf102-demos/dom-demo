import {MOUNTAINS} from "./data.js"

function createElementWithText(type, field) {
  const header = document.createElement(type);
  header.append(document.createTextNode(field));
  return header;
}

function createHeaderRow(fields) {
  const headerRow = document.createElement("tr");
  headerRow.append(...(fields.map(field => createElementWithText("th", field))));
  return headerRow;
}

function createRow(row, fields) {
  const tr = document.createElement("tr");
  tr.append(...(fields.map(field => createElementWithText("td", row[field]))));
  return tr;
}

function buildTable(data) {
  const table = document.createElement("table");
  const fields = Object.keys(data[0]);
  table.append(createHeaderRow(fields));
  table.append(...(data.map(row => createRow(row, fields))));
  return table
}

function setTable() {
  document.getElementById("mountains").append(buildTable(MOUNTAINS));
}

setTable();